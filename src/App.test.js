import {render, screen, waitForElementToBeRemoved} from '@testing-library/react';
import App from './App';

test('renders customers details', async () => {
  render(<App />);

  await waitForElementToBeRemoved(() => screen.getByTestId('loader'), {timeout: 2000})
  const linkElement = screen.getByTestId('customers-container');
  expect(linkElement).toBeInTheDocument();
});
