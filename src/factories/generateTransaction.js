import {faker} from "@faker-js/faker";

export const generateTransaction = (props) => ({
  id: faker.string.uuid(),
  amount: faker.number.float({min: 1, max: 200, precision: 2}),
  date: faker.date.past(),
  customer: faker.helpers.arrayElement(['Customer 1', 'Customer 2', 'Customer 3']),
  ...props
})