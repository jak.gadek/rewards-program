import {generateTransaction} from "../factories/generateTransaction";

export const getTransactions = (numOfTransactions) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      const sampleTransactions = Array.from({length: numOfTransactions}, generateTransaction);

      sampleTransactions.sort((a, b) => {
        const dateA = new Date(a.date);
        const dateB = new Date(b.date);
        return dateB - dateA;
      });

      resolve(sampleTransactions);
    }, 1000);
  });
}