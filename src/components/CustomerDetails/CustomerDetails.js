import {useMemo} from "react";
import {SummaryOfPoints} from "../SummaryOfPoints";
import {ListOfTransactions} from "../ListOfTransactions";
import {isDateWithinThreeMonths} from "../../utils/isDateWithinThreeMonths";
import './CustomerDetails.css'

export const CustomerDetails = ({transactions, customer}) => {
  const allowedTransactions = useMemo(() =>
      transactions.filter(({date}) => isDateWithinThreeMonths(date))
    , [transactions])

  return (
    <div>
      <h2>{customer}</h2>
      <div className='customer-details'>
        <div>
          <ListOfTransactions transactions={transactions}/>
        </div>
        <div className='summary'>
          <SummaryOfPoints transactions={allowedTransactions}/>
        </div>
      </div>
    </div>
  )
}