import logo from "../../logo.svg";
import './Loader.css'

export const Loader = () => <img src={logo} className="loader" alt="loader" data-testid='loader'/>