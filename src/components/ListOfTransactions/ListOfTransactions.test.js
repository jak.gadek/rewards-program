import React from 'react';
import {render, screen, within} from '@testing-library/react';
import {ListOfTransactions} from './ListOfTransactions';

jest.mock('../../utils/isDateWithinThreeMonths', () => ({
  isDateWithinThreeMonths: () => true
}));

describe('ListOfTransactions component', () => {
  const data = [
    {id: 1, date: '2023-10-05', amount: 120, points: 90},
    {id: 2, date: '2023-10-15', amount: 70, points: 20},
    {id: 3, date: '2023-10-15', amount: 100, points: 50},
  ];

  const transactions = data.map((el) => ({
    id: el.id,
    date: new Date(el.date),
    amount: el.amount,
  }))

  const testTransactionRow = (index) => {
    const transactionRow = screen.getByTestId(`transaction-${data[index].id}`);
    expect(transactionRow).toBeInTheDocument();

    within(transactionRow).getByText(data[index].amount)
    within(transactionRow).getByText(data[index].points)
  }

  test('renders table with correct transactions and points', () => {
    render(<ListOfTransactions transactions={transactions}/>);

    const tableElement = screen.getByTestId('list-of-transactions');
    expect(tableElement).toBeInTheDocument();

    testTransactionRow(0)
    testTransactionRow(1)
    testTransactionRow(2)
  });
});