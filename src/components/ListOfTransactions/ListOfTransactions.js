import {isDateWithinThreeMonths} from "../../utils/isDateWithinThreeMonths";
import {calculatePoints} from "../../utils/calculatePoints";
import './ListOfTransactions.css'

export const ListOfTransactions = ({transactions}) => (
  <>
    <table className='list-of-transactions' data-testid='list-of-transactions'>
      <thead>
      <tr>
        <th>
          Date
        </th>
        <th>
          Amount
        </th>
        <th>
          Points
        </th>
      </tr>
      </thead>
      <tbody>
      {transactions.map(({id, date, amount}) => {
        const isAllowed = isDateWithinThreeMonths(date);
        return (
          <tr key={id} className={isAllowed ? 'green' : ''} data-testid={`transaction-${id}`}>
            <td>
              {date.toLocaleString()}
            </td>
            <td>
              {amount}
            </td>
            <td>
              {isAllowed ? calculatePoints(amount) : ''}
            </td>
          </tr>
        )
      })
      }
      </tbody>
    </table>
  </>
)