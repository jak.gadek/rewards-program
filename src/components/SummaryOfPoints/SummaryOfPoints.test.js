import React from 'react';
import {render, screen} from '@testing-library/react';
import {SummaryOfPoints} from './SummaryOfPoints';

describe('SummaryOfPoints component', () => {
  const transactions = [
    {date: new Date('2023-10-05'), amount: 65},
    {date: new Date('2023-10-15'), amount: 55},
    {date: new Date('2023-11-08'), amount: 100},
  ];

  test('renders table with correct summary data', () => {
    render(<SummaryOfPoints transactions={transactions}/>);

    const tableElement = screen.getByText('Name');
    expect(tableElement).toBeInTheDocument();

    const summaryRow = screen.getByText('Summary').parentElement;
    expect(summaryRow).toHaveTextContent('70');

    const octoberPoints = screen.getByText('October').parentElement;
    expect(octoberPoints).toHaveTextContent('20');

    const novemberPoints = screen.getByText('November').parentElement;
    expect(novemberPoints).toHaveTextContent('50');
  });
});