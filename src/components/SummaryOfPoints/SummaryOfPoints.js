import {useMemo} from "react";
import {getMonthNameFromDate} from "../../utils/getMonthNameFromDate";
import {calculatePoints} from "../../utils/calculatePoints";
import './SummaryOfPoints.css'

const SUMMARY_TEXT = 'Summary';

export const SummaryOfPoints = ({transactions}) => {
  const monthlySummary = useMemo(() => {
    const pointsMap = new Map();

    transactions.forEach(({date, amount}) => {
      const month = getMonthNameFromDate(date);

      pointsMap.set(SUMMARY_TEXT, (pointsMap.get(SUMMARY_TEXT) || 0) + calculatePoints(amount))
      pointsMap.set(month, (pointsMap.get(month) || 0) + calculatePoints(amount))
    })

    return [...pointsMap.entries()]
  }, [transactions])

  return (
    <table className='summary-of-points'>
      <thead>
      <tr>
        <th>
          Name
        </th>
        <th>
          Points
        </th>
      </tr>
      </thead>
      <tbody>
      {monthlySummary.map(([key, value]) => (
        <tr key={key}>
          <td>{key}</td>
          <td>{value}</td>
        </tr>
      ))}
      </tbody>
    </table>
  )
}