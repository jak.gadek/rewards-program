export const calculatePoints = (amount) => {
  const roundedAmount = Math.floor(amount);

  let points = 0;

  if (roundedAmount > 100) {
    points += (roundedAmount - 100) * 2;  // 2 points for every dollar spent over $100 in each transaction
    points += 50; // Additional 50 points for the $50-$100 range
  } else if (roundedAmount > 50) {
    points += (roundedAmount - 50); // 1 point for every dollar between $50-$100
  }

  return points;
}