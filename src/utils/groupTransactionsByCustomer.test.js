import {generateTransaction} from "../factories/generateTransaction";
import {groupTransactionsByCustomer} from "./groupTransactionsByCustomer";

const firstCustomerTransactions = [
  generateTransaction({customer: 'firstCustomer'}),
  generateTransaction({customer: 'firstCustomer'}),
  generateTransaction({customer: 'firstCustomer'})
]

const secondCustomerTransactions = [
  generateTransaction({customer: 'secondCustomer'}),
  generateTransaction({customer: 'secondCustomer'}),
  generateTransaction({customer: 'secondCustomer'})
]

const transactions = [
  ...firstCustomerTransactions,
  ...secondCustomerTransactions
]

test('group transactions by customer', () => {
  expect(groupTransactionsByCustomer(transactions)).toMatchObject({
    firstCustomer: firstCustomerTransactions,
    secondCustomer: secondCustomerTransactions
  })
})