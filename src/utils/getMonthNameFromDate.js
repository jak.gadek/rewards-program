export const getMonthNameFromDate = (date) => {
  const monthNames = [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];
  const monthIndex = date.getMonth();
  return monthNames[monthIndex];
};