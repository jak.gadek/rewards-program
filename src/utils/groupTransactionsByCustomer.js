export const groupTransactionsByCustomer = (transactions) => {
  const groupedElements = {};

  transactions.forEach((element) => {
    const {customer} = element;
    if (!groupedElements[customer]) {
      groupedElements[customer] = [];
    }
    groupedElements[customer].push(element);
  });

  return groupedElements;
};