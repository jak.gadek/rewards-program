
import {getMonthNameFromDate} from "./getMonthNameFromDate";

test('get month name from date', () => {
  expect(getMonthNameFromDate(new Date('2023-01-01'))).toBe('January')
  expect(getMonthNameFromDate(new Date('2023-02-01'))).toBe('February')
  expect(getMonthNameFromDate(new Date('2023-03-01'))).toBe('March')
  expect(getMonthNameFromDate(new Date('2023-12-01'))).toBe('December')
})