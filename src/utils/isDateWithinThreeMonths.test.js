import {isDateWithinThreeMonths} from "./isDateWithinThreeMonths";

describe('get month name from date', () => {
  beforeAll(() => {
    jest.useFakeTimers();
    jest.setSystemTime(new Date(2023, 6, 1));
  });

  afterAll(() => {
    jest.useRealTimers();
  });

  test('dates', () => {
    expect(isDateWithinThreeMonths(new Date('2023-01-01'))).toBe(false)
    expect(isDateWithinThreeMonths(new Date('2023-02-01'))).toBe(false)
    expect(isDateWithinThreeMonths(new Date('2023-03-01'))).toBe(false)
    expect(isDateWithinThreeMonths(new Date('2023-04-01'))).toBe(true)
    expect(isDateWithinThreeMonths(new Date('2023-05-01'))).toBe(true)
    expect(isDateWithinThreeMonths(new Date('2023-06-01'))).toBe(true)
  })
})


