export const isDateWithinThreeMonths = (dateToCheck) => {
  const threeMonthsAgo = new Date();
  threeMonthsAgo.setMonth(threeMonthsAgo.getMonth() - 3);

  return dateToCheck >= threeMonthsAgo && dateToCheck <= new Date();
};