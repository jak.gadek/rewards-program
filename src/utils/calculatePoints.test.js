import {calculatePoints} from "./calculatePoints";

test('calculate points', () => {
  expect(calculatePoints(120)).toBe(90)
  expect(calculatePoints(120.09)).toBe(90)
  expect(calculatePoints(61)).toBe(11)
})