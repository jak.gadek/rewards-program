import './App.css';
import {useGetTransactions} from "./hooks/useGetTransactions";
import {Loader} from "./components/Loader";
import {useMemo} from "react";
import {CustomerDetails} from "./components/CustomerDetails";
import {groupTransactionsByCustomer} from "./utils/groupTransactionsByCustomer";

function App() {
  const {transactions, isFetched, isLoading} = useGetTransactions()

  const groupedTransactions = useMemo(() => groupTransactionsByCustomer(transactions), [transactions]);

  return (
    <div className="app">
      <div className="app-container" >
        {!isFetched || isLoading ? <Loader/> :
          <div className="customers-container" data-testid="customers-container">
            {
              Object.keys(groupedTransactions).map((customer) => (
                <div key={customer} className='app-box'>
                  <CustomerDetails customer={customer} transactions={groupedTransactions[customer]}/>
                </div>
              ))
            }
          </div>
        }
      </div>
    </div>
  );
}

export default App;
