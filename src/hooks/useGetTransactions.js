import {useEffect, useState} from "react";
import {getTransactions} from "../requests/getTransactions";

export const useGetTransactions = (numOfTransactions = 100) => {
  const [state, setState] = useState({
    transactions: [],
    isLoading: false,
    isFetched: false
  })

  useEffect(() => {
    setState((prevState) => ({
      ...prevState,
      isLoading: true
    }))

    getTransactions(numOfTransactions).then((data) => {
      setState({
        transactions: data,
        isLoading: false,
        isFetched: true
      })
    })

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return state
}